<?php
/**
 * Created by PhpStorm.
 * User: GMTORRES
 * Date: 6/7/2018
 * Time: 09:04
 */

namespace Centralpos\Carbon;


class Carbon extends \Carbon\Carbon
{
    /**
     * @var array
     */
    protected $noLaborableDays = [];

    /**
     * @return Carbon
     */
    public function nextLaborableday()
    {
        return $this->nextOrPreviousLaborableDay();
    }

    /**
     * @return Carbon
     */
    public function previousLaborableday()
    {
        return $this->nextOrPreviousLaborableDay(false);
    }

    /**
     * @return bool
     */
    public function isLaborable()
    {
        if (empty($this->noLaborableDays)) {
            $this->noLaborableDays = NoLaborables::get($this->year);
        }

        return !in_array($this->format('Y-m-d'), $this->noLaborableDays);
    }

    /**
     * @param bool $forward
     * @return Carbon
     */
    protected function nextOrPreviousLaborableDay($forward = true)
    {
        $step = $forward ? 1 : -1;

        do {
            $this->addDay($step);
        } while ($this->isWeekend() || !$this->isLaborable());

        return $this;
    }

    /**
     * @param $value
     * @return Carbon
     */
    protected function addOrSubLaborableDays($value)
    {
        $method = ($value >= 0) ? "addDay" : "subDay";
        $value = abs($value);
        for ($i = 0; $i < $value; $i++) {
            do {
                $this->{$method}();
            } while ($this->isWeekend() || !$this->isLaborable());
        }
        return $this;
    }

    /**
     * @param $value
     * @return Carbon
     */
    public function addLaborableDays($value)
    {
        return $this->addOrSubLaborableDays($value);
    }

    /**
     * @param $value
     * @return Carbon
     */
    public function subLaborableDays($value)
    {
        return $this->addOrSubLaborableDays(-1 * $value);
    }
}