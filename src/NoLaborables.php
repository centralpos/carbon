<?php
/**
 * Created by PhpStorm.
 * User: GMTORRES
 * Date: 6/7/2018
 * Time: 09:07
 */

namespace Centralpos\Carbon;

use GuzzleHttp\Client;

class NoLaborables
{
    /**
     * @param string $year
     * @return array
     */
    public static function get($year = null, $cache = true)
    {
        $year = $year ?: \Carbon\Carbon::now()->year;

        $cachePath = sys_get_temp_dir()."/feriados$year.json";

        if (!file_exists($cachePath) || !$cache || self::cacheNeedRefresh($cachePath)) {

            try {
                $jsonResponse = (new Client())
                    ->get("http://nolaborables.com.ar/api/v2/feriados/$year?incluir=opcional")
                    ->getBody();

                file_put_contents($cachePath, $jsonResponse);
            } catch (\Exception $e) {
                return [];
            }

        } else {
            $jsonResponse = file_get_contents($cachePath);
        }

        return self::formatResponse($jsonResponse, $year);
    }

    /**
     * @param  string $filepath
     * @return bool
     */
    protected static function cacheNeedRefresh($filepath)
    {
        $timestamp = filemtime($filepath);
        $createdDiff = \Carbon\Carbon::createFromTimestamp($timestamp)
            ->diffInHours(\Carbon\Carbon::today());

        return $createdDiff > 24;
    }

    /**
     * @param  string $jsonResponse
     * @param  string $year
     * @return array
     */
    protected static function formatResponse($jsonResponse, $year)
    {
        $feriados = [];

        foreach (json_decode($jsonResponse) as $feriado) {

            if (
                $feriado->tipo == 'nolaborable'
                && !isset($feriado->religion)
                || $feriado->tipo == 'nolaborable'
                && $feriado->religion != 'cristianismo'
            ) {
                continue;
            }

            $feriados[] = $year."-".str_pad($feriado->mes, 2, '0', STR_PAD_LEFT)
                ."-".str_pad($feriado->dia, 2, '0', STR_PAD_LEFT);
        }

        return $feriados;
    }
}